<?php
/**
* PHP class for Javascript Web Tokens (JWT)
* @copyright 2016 Philip K Meadows, All Rights Reserved
* @author Philip K Meadows
* 
* ##### REDISTRIBUTION #####
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice and notes, this list of conditions
* AND the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice and notes, this list of conditions
* AND the following disclaimer in the documentation and/or other materials provided with the distribution.
* 
* ##### DISCLAIMER #####
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
* feel free to comment out the namespace or rename it to suit your needs
*/
namespace WebGuyPhil\PHPJWT;

/**
* feel free to comment out or rename the "use" statement to suit your needs
* the IMPORTANT THING is that your app/site has access to the PHPCryptography class
* get it here: https://bitbucket.org/pmeadows/php-cryptography
* you could access it by a PHP "require/include" as an alternative
*/
use WebGuyPhil\PHPCryptography;

use DateTime;
use DateTimeZone;
use DateInterval;

class PHPJWT extends PHPCryptography
{
	
	protected $appSalt;
	protected $dateObj;
	protected $tknLifetime;
	protected $timezone;
	protected $tknIssued;
	protected $tknExpires;
	
	public function __construct()
	{
		// class parameters, edit to suit your needs...
		$this->timezone = 'Europe/London'; // see http://php.net/manual/en/timezones.php
		$this->tknLifetime = 1800; // in seconds e.g. 1800sec = 30min (30 * 60)
		
		// call the parent constructor
		parent::__construct();
		
		// we need a consistent salt, best you set this as an environment variable
		// .htaccess is a good place to do this, or in Laravel your .env file
		// TO DO: perhaps auto generate a salt and save in file outside public dir with 0444 permissions ?
		if( ! isset( $_ENV['APP_SALT'] ) )
		{
			trigger_error( $this->theClass . ' ERROR: No app salt exists!', E_USER_ERROR );
		}
		else
		{
			$this->appSalt = $_ENV['APP_SALT'];
		}
		
		$objTknIssued = new DateTime( 'NOW', new DateTimeZone( $this->timezone ) );
		$this->tknIssued = $objTknIssued->getTimestamp();
		$objTknExpires = new DateTime( 'NOW', new DateTimeZone( $this->timezone ) );
		$this->tknExpires = $objTknExpires->add( new DateInterval( 'PT'.$this->tknLifetime.'S' ) )->getTimestamp();
	} // END __construct()
	
	/**
	* 
	* @param string $data: The string to encode
	* 
	* @return string: base64 encoded string in URL safe format
	*/
	private function b64UrlEncode( $data )
	{ 
		return rtrim( strtr( base64_encode( $data ), '+/', '-_' ), '=' ); 
	} // END b64UrlEncode()

	/**
	* 
	* @param string $data: The URL safe base64 string
	* 
	* @return string: raw base64 URL UNSAFE string
	*/
	private function b64UrlDecode( $data )
	{ 
		return base64_decode( str_pad( strtr( $data, '-_', '+/' ), strlen( $data ) % 4, '=', STR_PAD_RIGHT ) ); 
	} // END b64UrlDecode()
	
	
	public function issue( array $publicClaims = NULL )
	{
		// build the header
		$jwtHeader = array(
			'typ' => 'JWT',
			'alg' => $this->algorithm,
		);
		$jwtHeader = json_encode( $jwtHeader );
		$jwtHeader = $this->b64UrlEncode( $jwtHeader );
		
		// build the payload
		$registerdClaims = array(
			'iss' => $_SERVER['HTTP_HOST'],
			'exp' => $this->tknExpires,
			'iat' => $this->tknIssued,
		);
		$jwtPayload = $registerdClaims;
		if( $publicClaims !== NULL )
		{
			$jwtPayload = array_merge( $jwtPayload, $publicClaims );
		}
		$jwtPayload = json_encode( $jwtPayload );
		$jwtPayload = $this->b64UrlEncode( $jwtPayload );
		
		// build the signature
		$signature = $this->b64UrlEncode( $this->encrypt( $jwtHeader.'.'.$jwtPayload, $this->appSalt ) );
		
		// compile the token
		$tkn = $jwtHeader.'.'.$jwtPayload.'.'.$signature;
		
		return $tkn;
		
	} // END issue()
	
	/**
	* 
	* @param string $tkn: The passed JWT
	* @param bool $bool: Whether to return boolean response OR array of data
	* 
	* @return bool: Whether a valid JWT or not
	* OR
	* @return array: 'status' => boolean: as above, 'message' -> string: response message
	*/
	public function isValid( $tkn, $bool = TRUE )
	{
		// check there IS a token
		if( strlen( $tkn ) == 0 )
		{
			return $bool == TRUE ? FALSE : array( 'status' => FALSE, 'message' => 'No token!' );
		}
		
		// count the parts
		$parts = explode( '.', $tkn );
		if( count( $parts ) !== 3 )
		{
			return $bool == TRUE ? FALSE : array( 'status' => FALSE, 'message' => 'Corrupt token structure!' );
		}
		
		// compare the header + payload with the decrypted signature
		$passedHeaderPlusPayload = $parts[0] . '.' . $parts[1];
		$decryptedSignature = $this->decrypt( $this->b64UrlDecode( $parts[2] ), $this->appSalt );
		if( $passedHeaderPlusPayload !== $decryptedSignature )
		{
			return $bool == TRUE ? FALSE : array( 'status' => FALSE, 'message' => 'Signature invalid!' );
		}
		
		// check the token is not expired
		$signedHeaderPlusPayload = explode( '.', $decryptedSignature );
		$signedPayload = json_decode( $this->b64UrlDecode( $signedHeaderPlusPayload[1] ) );
		$expires = $signedPayload->exp;
		$objNow = new DateTime( 'NOW', new DateTimeZone( $this->timezone ) );
		$now = $objNow->getTimestamp();
		if( $now > $expires )
		{
			return $bool == TRUE ? FALSE : array( 'status' => FALSE, 'message' => 'Token expired!' );
		}		
		
		// you got this far? Well done
		return $bool == TRUE ? TRUE : array( 'status' => TRUE, 'message' => 'Good stuff!' );
		
	} // END isValid()
	
	public function getClaim( $tkn, $claim )
	{
		// check tkn is valid
		if( $this->isValid( $tkn ) )
		{
			$parts = explode( '.', $tkn );
			$decryptedSignature = $this->decrypt( $this->b64UrlDecode( $parts[2] ), $this->appSalt );
			$signedHeaderPlusPayload = explode( '.', $decryptedSignature );
			$signedPayload = json_decode( $this->b64UrlDecode( $signedHeaderPlusPayload[1] ) );
			if( property_exists( $signedPayload, $claim ) )
			{
				return $signedPayload->$claim;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			// token not valid
			return FALSE;
		}
		
	} // END getClaim()
	
} // END class PHPJWT
?>